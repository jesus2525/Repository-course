import { useState } from "react"
import { AddCategory, GifGrid } from "./components";



export const GifExpertApp= () => {

    const [ categories, setCategorias] = useState(['One Punch']);
    
    const onAddCategory = (NewCategory) => {
        // categories1.push(NewCategory); no se debe hacer
        if(categories.includes(NewCategory)) return;
        setCategorias([ NewCategory,...categories]);
        // setCategorias1([ ...categories1, 'Naruto']);
        // setCategorias1(cat=>[ 'Naruto',...categories1]);
    }
    return (
        <>
        {/*Título*/}
            <h1>GifExpertApp</h1>
        {/*Título*/}
        <AddCategory 
        onNewCategory={event =>onAddCategory(event) }
        // setCategories={setCategorias1} 
        />
        {/*Título*/}
        {/* <{button onClick={event => onSubmit(event)}>Agregar</button>} */}
                {
                    categories.map((category) =>(
                        <GifGrid 
                        key={category}
                        category={category}
                        />
                    ))
                }
        </>
    )
}