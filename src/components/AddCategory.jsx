import { useState } from "react"
// import { GifExpertApp } from "../GifExpertApp";


 export const AddCategory = ({onNewCategory}) => {

    const [inputValue,setInputValue] = useState('');
    
    //target es un objeto que se obtiene del evento, se está desestructurando
    const onInputChange = ({target}) => {
        setInputValue(target.value)
    }
    const onSubmit = (event) => {
        event.preventDefault();//para que no se haga el refresh del navegador
        // console.log(inputValue);
        
        if(inputValue.trim().length<=1) return;
        // setCategories(categories1=> [inputValue, ...categories1]);
        onNewCategory(inputValue.trim());
        setInputValue('');
    }

    return (
        <form onSubmit={event => onSubmit(event)}>
            <input
            type = 'text'
            placeholder="Buscar Gifs"
            value={inputValue}
            onChange={onInputChange}
        >
        </input>
        </form>
        
    )
 }